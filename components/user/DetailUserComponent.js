import React, { Component } from 'react'
import { Text, View, AsyncStorage, Image, StyleSheet } from 'react-native'
import { Appbar, Headline, Title, Button } from 'react-native-paper'


export class DetailUserComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token: ''
        };
        this._retrieveData();
    }

    _retrieveData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            if (token !== null) {
                this.setState({ token })
            }
        } catch (error) {
            console.error("🚫" + error);
        }
    };

    render() {
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />                           
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>
                    </Image>
                    <View style={styles.vue} />
                </Appbar.Header>
                <View style={styles.headlineGroup}>
                    <Headline style={styles.headline}>Mon compte</Headline>
                </View>
                <View style={ styles.userInfos }>
                    <Title>Pseudo : Sanvoisins</Title>
                    <Title>Prénom : Antoine</Title>
                    <Title>Nom de famille : Sanvoisin</Title>
                    <Title>Adresse mail : Sanvoisin</Title>
                    <Title>Age : Sanvoisin</Title>
                </View>
                <View style={styles.buttonGroup}>
                    <Button 
                        mode='outlined' 
                        color='black'
                        style={styles.button}
                        
                    >
                        Modifier mes informations
                    </Button>
                    <Button
                        mode='outlined' 
                        color='black' 
                        style={styles.button}
                    >
                        Supprimer mon compte
                    </Button>
                    <Button 
                        mode='outlined' 
                        color='black' 
                        style={styles.button}
                    >
                        Me déconnecter
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    logo: {
        height: 40,
        width: '36%',
    },
    vue: {
        height: '20%',
        width: 30
    },
    userInfos: {
        margin: 10,
        padding: 10,
        borderRadius: 10,
        borderWidth: 1
    },
    headline: {
        fontSize : 30,
        fontWeight : 'bold'
    },
    button: {
        width: '70%',
        margin: 5,
        borderWidth: 1,
    },
    buttonGroup: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headlineGroup: {
        padding: 4,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default DetailUserComponent
