import React, { Component } from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import { Appbar, TextInput, Button, Headline} from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';

export class AddBarComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          description:'',
          adress: '',
        };
      }
    render() {
        return (
        <View style={styles.page}>
            <Appbar.Header style={styles.header}>     
            <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />                           
                        <Image
                            source={require('../../assets/logo.png')}
                            style={styles.logo}>
                                
                        </Image>
                    <View style={styles.vue} />
            </Appbar.Header>

            <View style={styles.parent}>
                <View style={styles.child0}>
                    <Headline style={{fontWeight : 'bold', textDecorationLine : 'underline', fontSize: 32, height: 50, justifyContent : 'center', flexDirection: 'column' }}> 
                    Modifier un bar
                    </Headline>
                </View>
                <ScrollView>
                    <View style={styles.child1}>
                        <TextInput
                            style={styles.text}
                            label='Pseudo'
                            value={this.state.name}
                            onChangeText={text => this.setState({ name: text })}
                        />                    
                        <TextInput
                            style={styles.text}
                            label='Adresse'
                            value={this.state.adress}
                            onChangeText={text => this.setState({ adress: text })}
                        />
                        <TextInput
                            style={styles.longText}
                            label='Description'
                            value={this.state.description}
                            multiline
                            onChangeText={text => this.setState({ description: text })}
                        />
                    </View>
                </ScrollView>
                <View style={styles.child2}>
                    <View style={styles.litleChild1}>
                        <Button style={styles.button} mode="contained" onPress={() => this._registerUser()}>
                            Annuler
                        </Button>       
                    </View>
                    <View style={styles.litleChild2}>
                        <Button style={styles.button} mode="contained" onPress={() => this._registerUser()}>
                            Confirmer
                        </Button>
                    </View>
                </View>
            </View>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    logo: {
        height: 40,
        width: '36%',      
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    vue: {
        height: '20%',
        width: '10%',
    },
    parent: {
        flex: 1,
        width: '100%',
        height: 500,
        alignItems: 'center',
    },
    child0: {
        width: '100%',
        height: '30%',
        flexDirection: 'column',
        justifyContent : 'center',
        alignItems: 'center',
    },
    child1: {
        flex: 1,
        width: '100%',
        height: 400,
        justifyContent: 'center',
        alignItems: 'center',
    },
    child2: {
        width: '100%',
        height: 200,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection : 'row'
    },
    litleChild1 : {
        width: 150,
        height: 70,
    },
    litleChild2 : {
        width: 150,
        height: 70,
    },
    text: {
        marginTop: 20,
        width: 300,
        height: 60
      },
      longText: {
        marginTop: 20,
        width: 300,
        height: 180
      },
      button: {
        height: 60,
        justifyContent: 'center'
      }
})

export default AddBarComponent
