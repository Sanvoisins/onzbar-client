import React, { Component } from 'react'
import { StyleSheet, View, Image, AsyncStorage } from 'react-native'
import { Appbar, List } from 'react-native-paper'
import urlApi from '../../assets/api'
import * as axios from 'axios'

export class FavBarComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token: '',
            fav: []
        };
        this._retrieveData();
    }

    _retrieveData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            if (token !== null) {
                this.setState({ token })
            }
        } catch (error) {
            console.error("🚫 " + error);
        }
    }

    _GetFavorites = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.get(urlApi + 'favorites/', axiosConfig)
        .then((response) => {
            this.setState({fav: response.data.favorites})
        })
        .catch((error) => {
            console.log("🚫 " + error);
            this.setState({
                errorMessage: 'Problèmes d\'affichage des données'
            });
        });
    }

    componentDidMount = () => {
        setTimeout(() => {this._GetFavorites()}, 1000);
    }

    render() {
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />                           
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>
                            
                    </Image>
                    <View style={styles.vue} />
                </Appbar.Header>
                {
                    this.state.fav.map((bar) => {
                        return(
                            <List.Item
                                key={bar.bar_id}
                                title={bar.name}
                                description={bar.adress}
                                left={props => <List.Icon {...props} icon="beer" />}
                                onPress={() => {this.props.navigation.navigate('DetailBar', { idBar: bar.bar_id })}}
                            />
                        )
                    })
                }
            </View>
        )
    }
}
const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    logo: {
        height: 40,
        width: '36%',
    },
    vue: {
        height: '20%',
        width: 30
    }
});

export default FavBarComponent

