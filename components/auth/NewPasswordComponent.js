import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage } from 'react-native';
import { Appbar, Button, TextInput, Title } from 'react-native-paper';

class SignInComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
        password: '',
        confirmedPassword:'',
    };
  }

  render() {
    return (
      <View style={styles.container}>
      <Appbar.Header>
            <Appbar.Content
                title="STONED"
                subtitle="Changement de mot de passe"
            />
      </Appbar.Header>
        <View style={styles.top}>
          <Title style={styles.errorMessage}>{ this.state.errorMessage }</Title>
          <TextInput
            style={styles.text}
            label='Mot de passe'
            value={this.state.password}
            onChangeText={text => this.setState({ password: text })}
            secureTextEntry={true}
          />
          <TextInput
            style={styles.text}
            label='Confirmer mot de passe'
            value={this.state.confirmedPassword}
            onChangeText={text => this.setState({confirmedPassword: text })}
            autoCompleteType="password"
            textContentType="password"
            secureTextEntry={true}
          />
    </View>
    <View style={styles.center}>
      <Button mode="contained" onPress={() => this.login()}>
        Se connecter
      </Button>
    </View>
    <View style={styles.bottom}>
      <Button mode="outlined" onPress={() => this.props.navigation.navigate('Signin')}>
        S'inscrire
      </Button>

    </View>
  </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'white'
  },
  top: {
    marginTop: 100,
    width: '100%',
    height: '33.33%',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 20
  },
  center: {
    width: '100%',
    height: '33.33%',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 20
  },
  bottom: {
    width: '100%',
    height: '33.33%',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  text: {
    marginTop: 20,
    width: 300,
    height: 50,
  },
  errorMessage: {
    color: 'red'
  }
});

export default SignInComponent;
