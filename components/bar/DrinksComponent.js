import React, { Component } from 'react'
import { StyleSheet, View, Image, AsyncStorage } from 'react-native'
import { Appbar, List, Button, Headline, TextInput, Title } from 'react-native-paper'
import urlApi from '../../assets/api'
import * as axios from 'axios'
import Modal from 'react-native-modal';

export class DrinksComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            drinks: [],
            token: '',
            isVisibleRegister: false,
            isVisibleDelete: false,
            name: '',
            description: '',
            price: '',
            price_happyhour: '',
            drinkId: ''
        };
        this._retrieveData();
    }


    htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    _retrieveData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            if (token !== null) {
                this.setState({ token })
            }
        } catch (error) {
            console.error("🚫 " + error);
        }
    };

    _GetDrinks = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.get(urlApi + 'drinks/' + this.props.route.params.idBar, axiosConfig)
            .then((response) => {
                this.setState({ drinks: response.data.drinks })
            })
            .catch((error) => {
                console.log("🚫" + error);
                this.setState({
                    errorMessage: 'Problèmes d\'affichage des données'
                });
            });
    }

    _RegisterDrinks = () => {
        const data = this.state;
        if (data.name !== "" && data.description !== "" && data.price !== "" && data.price_happyhour !== "") {
            const newDrinks = {
                "name": this.htmlEntities(data.name),
                "description": this.htmlEntities(data.description),
                "price": this.htmlEntities(data.price),
                "price_happyhour": encodeURIComponent(data.price_happyhour.trim())
            };
            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': this.state.token
                }
            };
            axios.post(urlApi + 'drinks/' + this.props.route.params.idBar, newDrinks, axiosConfig)
                .then((response) => {
                    this.props.navigation.navigate('DetailBar', { idBar: this.props.route.params.idBar });
                })
                .catch(function (error) {
                    console.log("🚫" + error);
                });
        } else {
            this.setState({ errorMessage: 'Vous n\'avez pas tous rempli' })
        }
    }

    _DeleteDrinks = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.delete(urlApi + 'drinks/' + this.state.drinkId, axiosConfig)
            .then((response) => {
                this.props.navigation.navigate('DetailBar', { idBar: this.props.route.params.idBar });
            })
            .catch(function (error) {
                console.log("🚫" + error);
            });
    }

    componentDidMount = () => {
        setTimeout(() => { this._GetDrinks() }, 1000);
    };

    render() {
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>

                    </Image>
                    <Appbar.Action
                        icon="plus"
                        onPress={() => { this.setState({ isVisibleRegister: !this.state.isVisibleRegister }) }}
                    />
                </Appbar.Header>
                <Modal isVisible={this.state.isVisibleRegister} style={styles.modalContainer}>
                    <View style={styles.modal}>
                        <View style={styles.title}>
                            <Headline>Ajout d'une boisson</Headline>
                        </View>
                        <TextInput
                            label="Nom"
                            value={this.state.name}
                            onChangeText={(text) => { this.setState({ name: text }) }}
                            style={styles.textInput}
                        />
                        <TextInput
                            label="Description"
                            value={this.state.description}
                            onChangeText={(text) => { this.setState({ description: text }) }}
                            style={styles.textInput}
                        />
                        <TextInput
                            label="Prix hors Happy Hour"
                            value={this.state.price}
                            onChangeText={(text) => { this.setState({ price: text }) }}
                            style={styles.textInput}
                        />
                        <TextInput
                            label="Prix en Happy Hour"
                            value={this.state.price_happyhour}
                            onChangeText={(text) => { this.setState({ price_happyhour: text }) }}
                            style={styles.textInput}
                        />
                        <Button mode='contained' color='black' style={styles.button} onPress={() => { this._RegisterDrinks() }} > Enregistrer </Button>
                        <Button mode='contained' color='black' style={styles.button} onPress={() => { this.setState({ isVisibleRegister: !this.state.isVisibleRegister }) }} > Annuler </Button>
                    </View>
                </Modal>
                <Modal isVisible={this.state.isVisibleDelete} style={styles.modalContainer}>
                    <View style={styles.modal}>
                        <View style={styles.title}>
                            <Title>Êtes vous sur de vouloir supprimer la boisson ?</Title>
                        </View>
                        <Button mode='contained' color='red' style={styles.button} onPress={() => { this._DeleteDrinks() }} > Supprimer </Button>
                        <Button mode='contained' color='black' style={styles.button} onPress={() => { this.setState({ isVisibleDelete: !this.state.isVisibleDelete }) }} > Annuler </Button>
                    </View>
                </Modal>
                {
                    this.state.drinks.map((item) => (
                        <List.Item
                            key={item.id}
                            title={item.name}
                            description={`Happy Hour : ${item.price_happyhour}  Prix normal : ${item.price}`}
                            left={props => <List.Icon {...props} icon="beer" />}
                            right={props => <List.Icon {...props} icon="trash-can-outline" />}
                            onPress={() => { this.setState({ isVisibleDelete: !this.state.isVisibleDelete, drinkId: item.id }) }}
                        />
                    ))
                }
            </View>
        )
    }
}
const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    logo: {
        height: 40,
        width: '36%',
    },
    vue: {
        height: '20%',
        width: 30
    },
    modalContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        marginTop: 20,
        width: 300,
        height: 50,
        margin: 5
    },
    button: {
        margin: 5
    },
    title: {
        backgroundColor: '#F2F2F2',
        padding: 5,
        borderWidth: 1,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default DrinksComponent

