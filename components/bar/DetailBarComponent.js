import React, { Component } from 'react'
import { Text, View, StyleSheet, AsyncStorage, Image } from 'react-native'
import { Appbar, Title, Subheading, Headline, TextInput, Button } from 'react-native-paper'
import urlApi from '../../assets/api';
import { Divider, ListItem } from 'react-native-elements'
import * as axios from 'axios'
import Modal from 'react-native-modal';

const list = [
    {
        title: "Horaire d'ouverture",
        subtitle: '10h00 - 1h30',
        icon: 'schedule'

    },
    {
        title: 'Happy Hour',
        subtitle: '16h30 - 1h30',
        icon: 'alarm'
    },
    {
        title: 'Description',
        subtitle: "Un bar avec une bonne ambiance et une très bonne happy hour. Viens solo ou avec tes potes tu t'y plaira",
        icon: 'description'
    }
]

const list2 = [
    {
        title: "Liste des boissons",
        icon: 'local-bar'
    }
]

const list3 = [
    {
        title: "Boisson la - chère",
        subtitle: 'Kronenbourg : \n - Happy Hour : 3€ \n - Prix Normal : 5€',

    }

]

export class DetailBarComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token: '',
            bar: {},
            isFavoris: false,
            isVisibleFav: false,
            image: 'https://via.placeholder.com/150',
            note: '',
            avgGrades: ''
        };
        this._retrieveData();
    }

    _retrieveData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            if (token !== null) {
                this.setState({ token })
            }
        } catch (error) {
            console.error("🚫" + error);
        }
    };

    _AddFavorite = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.post(urlApi + 'favorites/' + this.props.route.params.idBar, {}, axiosConfig)
            .then((response) => {
                this.setState({ isFavoris: true });
            })
            .catch(function (error) {
                console.log("🚫" + error);
            });
    }

    _DeleteFavorite = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.delete(urlApi + 'favorites/' + this.props.route.params.idBar, axiosConfig)
            .then((response) => {
                this.setState({ isFavoris: false });
            })
            .catch(function (error) {
                console.log("🚫" + error);
            });
    }

    _isFavorite = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.get(urlApi + 'favorites/', axiosConfig)
            .then((response) => {
                response.data.favorites.forEach(element => {
                    if (element.bar_id == this.props.route.params.idBar) {
                        this.setState({ isFavoris: true });
                    }
                });
            })
            .catch((error) => {
                console.log("🚫 " + error);
                this.setState({
                    errorMessage: 'Problèmes d\'affichage des données'
                });
            });
    }

    _GetBar = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.get(urlApi + 'bars/' + this.props.route.params.idBar, axiosConfig)
            .then((response) => {
                this.setState({ bar: response.data.bar })
            })
            .catch((error) => {
                console.log("🚫" + error);
                this.setState({
                    errorMessage: 'Problèmes d\'affichage des données'
                });
            });
    }

    _GetAvgGrades = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.get(urlApi + 'grades/' + this.props.route.params.idBar, axiosConfig)
            .then((response) => {
                this.setState({ avgGrades: response.data.avg })
            })
            .catch((error) => {
                console.log("🚫" + error);
                this.setState({
                    errorMessage: 'Problèmes d\'affichage des données'
                });
            });
    }

    _RegisterGade = () => {
        const newNote = {
            "evaluation": this.state.note
        };
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.post(urlApi + 'grades/' + this.props.route.params.idBar, newNote, axiosConfig)
            .then((response) => {
                this.props.navigation.navigate('DetailBar', { idBar: this.props.route.params.idBar });
            })
            .catch(function (error) {
                console.log("🚫" + error);
            });
    }

    componentDidMount = () => {
        setTimeout(() => {
            this._GetBar()
            this._isFavorite()
            this._GetAvgGrades()
        }, 1000);
    }

    render() {
        return (

            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>

                    </Image>
                    <View style={styles.vue} />
                </Appbar.Header>
                <View style={styles.parent}>
                    <View style={styles.childOne}>

                        <View style={styles.littleChildOne}>
                            <Headline style={styles.headline}>{this.state.bar.name}</Headline>
                            <Subheading>{this.state.bar.adress}</Subheading>
                        </View>
                        <View style={styles.littleChildTwo}>
                            <ListItem
                                title="Horaire d\'ouverture"
                                subtitle={this.state.bar.openhour}
                                leftIcon={{ name: 'schedule' }}
                                titleStyle={{ fontWeight: 'bold' }}
                                style={styles.list1}
                            />
                            <ListItem
                                title='Happy Hour'
                                subtitle={this.state.bar.happyhour}
                                leftIcon={{ name: 'alarm' }}
                                titleStyle={{ fontWeight: 'bold' }}
                                style={styles.list1}
                            />
                            <ListItem
                                title='Description'
                                subtitle={this.state.bar.description}
                                leftIcon={{ name: 'description' }}
                                titleStyle={{ fontWeight: 'bold' }}
                                style={styles.list1}
                            />
                        </View>
                        <View style={styles.littleChildThree}>
                            {
                                list2.map((item, i) => (
                                    <ListItem
                                        key={i}
                                        title={item.title}
                                        subtitle={item.subtitle}
                                        leftIcon={{ name: item.icon }}
                                        titleStyle={{ fontWeight: 'bold' }}
                                        onPress={() => { this.props.navigation.navigate('Drinks', { idBar: this.props.route.params.idBar }) }}
                                        chevron
                                    />
                                ))
                            }
                            {
                                list3.map((item, i) => (
                                    <ListItem
                                        key={i}
                                        title={item.title}
                                        subtitle={item.subtitle}
                                        leftIcon={{ name: item.icon }}
                                        titleStyle={{ fontWeight: 'bold' }}
                                    />
                                ))
                            }
                        </View>
                        <Modal isVisible={this.state.isVisibleFav} style={styles.modalContainer}>
                            <View style={styles.modal}>
                                <View style={styles.title}>
                                    <Title>Noter le bar</Title>
                                </View>
                                <TextInput
                                    label="Notes"
                                    value={this.state.note}
                                    onChangeText={(text) => { this.setState({ note: text }) }}
                                    style={styles.textInput}
                                />
                                <Button mode='contained' color='green' style={styles.button} onPress={() => { this._RegisterGade() }} > Enregistrer </Button>
                                <Button mode='contained' color='black' style={styles.button} onPress={() => { this.setState({ isVisibleFav: !this.state.isVisibleFav }) }} > Annuler </Button>
                            </View>
                        </Modal>
                        <View style={styles.littleChildFour}>
                            <Button
                                icon="star"
                                style={styles.button}
                                mode='outlined'
                                color='black'
                            >
                                {this.state.avgGrades}
                            </Button>
                            <Button
                                mode='outlined'
                                style={styles.button}
                                color='black'
                                onPress={() => { this.setState({ isVisibleFav: !this.state.isVisibleFav }) }}
                            >
                                Noter le bar
                            </Button>

                        </View>
                        <View style={styles.littleChildFive}>
                            {this.state.isFavoris ?
                                <Button
                                    mode='outlined'
                                    style={styles.button}
                                    color='black'
                                    fontWeight='bold'
                                    onPress={() => { this._DeleteFavorite() }}
                                >
                                    Supprimer des favoris
                                </Button>
                                :
                                <Button
                                    mode='outlined'
                                    style={styles.button}
                                    color='black'
                                    fontWeight='bold'
                                    onPress={() => { this._AddFavorite() }}
                                >
                                    Ajouter aux favoris
                            </Button>
                            }
                        </View>
                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    image: {
        width: '100%',
        height: 200
    },
    parent: {
        padding: 4,
        height: '100%',
        width: '100%',
        flexDirection: 'column'
    },
    childOne: {
        width: '100%',
        height: '22%',
        alignItems: 'center'
    },
    littleChildOne: {
        width: '100%',
        height: '30%',
        alignItems: 'center'
    },
    littleChildTwo: {
        padding: 2,
        width: '100%',
        height: '130%'
    },
    littleChildThree: {
        padding: 2,
        width: '100%',
        height: '100%'
    },
    littleChildFour: {
        height: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    littleChildFive: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    headline: {
        fontWeight: 'bold',
        fontSize: 32,
        height: 25,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    icon: {
        width: 100
    },
    star: {
        width: 100
    },
    buttonDrinks: {
        margin: 3,
        borderColor: 'black',
        height: '100%',
    },
    button: {
        margin: 3,
        borderColor: 'black'
    },
    list1: {
        margin: 3
    },
    list2: {
        margin: 1
    },
    vue: {
        height: '20%',
        width: '10%',
    },
    logo: {
        height: 40,
        width: '36%',
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    button: {
        margin: 5
    },
    title: {
        backgroundColor: '#F2F2F2',
        padding: 5,
        borderWidth: 1,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        marginTop: 20,
        width: 300,
        height: 50,
        margin: 5
    },
    modal: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
})

export default DetailBarComponent
