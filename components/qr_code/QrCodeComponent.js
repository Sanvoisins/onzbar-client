import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Appbar, Button, Title } from 'react-native-paper';
import { BarCodeScanner } from 'expo-barcode-scanner';

export default function App({ navigation }) {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [errorQr, setErrorQr] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        if (data.substring(0, 7) === "onzbar_") {
            navigation.navigate('DetailBar', { idBar: data.substring(7) })
        } else {
            setErrorQr(true);
        }
    };
    const reScanAfterError = () => {
        setScanned(false);
        setErrorQr(false);
    }
    if (hasPermission === null) {
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => navigation.goBack()} />
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>

                    </Image>
                    <Appbar.Action
                        icon="star"
                        onPress={() => navigation.navigate('FavBar')}
                    />
                </Appbar.Header>
                <Title style={styles.infos}>⚠️ Demande de permissions ⚠️ </Title>
            </View>
        )
    }
    if (hasPermission === false) {
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => navigation.goBack()} />
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>

                    </Image>
                    <Appbar.Action
                        icon="star"
                        onPress={() => navigation.navigate('FavBar')}
                    />
                </Appbar.Header>
                <Title style={styles.infos}>⚠️ Accés à la caméra refusé ⚠️</Title>
            </View>
        )
    }
    if (errorQr) {
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => navigation.goBack()} />
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>

                    </Image>
                    <Appbar.Action
                        icon="star"
                        onPress={() => navigation.navigate('FavBar')}
                    />
                </Appbar.Header>
                <Title style={styles.infos}>⚠️ QRCode incompatible !! ⚠️ </Title>
                <Button style={styles.reScan1} icon="camera" mode="contained" onPress={() => reScanAfterError(false)}> Appuyer pour re-scanner</Button>
            </View>
        )
    }
    return (
        <View style={styles.page}>
            <Appbar.Header style={styles.header}>
                <Appbar.BackAction onPress={() => navigation.goBack()} />
                <Image
                    source={require('../../assets/logo.png')}
                    style={styles.logo}>

                </Image>
                <Appbar.Action
                    icon="star"
                    onPress={() => navigation.navigate('FavBar')}
                />
            </Appbar.Header>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={styles.map}
            />
            {scanned && <Button style={styles.reScan2} icon="camera" mode="contained" onPress={() => setScanned(false)}> Appuyer pour re-scanner</Button>}
        </View>
    );
}

const styles = StyleSheet.create({
    reScan1: {
        marginTop: '90%',
        margin: '10%'
    },
    reScan2: {
        marginTop: '-20%',
        margin: '10%'
    },
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    map: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: 'yellow'
    },
    infos: {
        position: 'absolute',
        height: '25%',
        width: '90%',
        marginTop: '100%',
        margin: '18%',
        color: 'red',
        fontWeight: 'bold'
    },
    logo: {
        height: 40,
        width: '36%',
    },
    vue: {
        height: '20%',
        width: '10%',
    },
    fab: {
        backgroundColor: 'black'
    }
})