import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, AsyncStorage } from 'react-native'
import { Appbar, Avatar, Button, Card, FAB } from 'react-native-paper'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import urlApi from '../assets/api'
import * as axios from 'axios'


class HomeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisibleInfos: false,
            bars: [],
            token: '',
            barInfo: {},
            fabOpen: false,
            latitude: 48.8534,
            longitude: 2.3488
        };
        this._retrieveData();
    }

    manageInfos = (type, bar) => {
        switch (type) {
            case 'close':
                this.setState({ isVisibleInfos: false })
                break;
            case 'open':
                this.setState({ isVisibleInfos: true, barInfo: bar, latitude: +bar.latitude, longitude: +bar.longitude })
                break;
            default:
                break;
        }
    }

    _retrieveData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            if (token !== null) {
                this.setState({ token })
            }
        } catch (error) {
            console.error("🚫" + error);
        }
    };

    _GetBars = () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': this.state.token
            }
        };
        axios.get(urlApi + 'bars/', axiosConfig)
            .then((response) => {
                this.setState({ bars: response.data.bars })
            })
            .catch((error) => {
                console.log("🚫" + error);
                this.setState({
                    errorMessage: 'Problèmes d\'affichage des données'
                });
            });
    }

    componentDidMount = () => {
        setTimeout(() => { this._GetBars() }, 1000);
    }

    render() {
        const LeftContent = props => <Avatar.Icon {...props} icon='beer' backgroundColor='black' />
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
                    <Image
                        source={require('../assets/logo.png')}
                        style={styles.logo}>

                    </Image>
                    <Appbar.Action
                        icon="star"
                        onPress={() => this.props.navigation.navigate('FavBar')}
                    />
                </Appbar.Header>
                <MapView
                    provider={PROVIDER_GOOGLE}
                    style={styles.map}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.10,
                        longitudeDelta: 0.10,
                    }}
                >
                    {
                        this.state.bars.map((bar) => {
                            return (
                                <MapView.Marker
                                    key={bar.bar_id}
                                    coordinate={{ longitude: +bar.longitude, latitude: +bar.latitude }}
                                    pinColor={'black'}
                                    onPress={() => { this.manageInfos('open', bar) }}
                                />
                            )
                        })
                    }
                </MapView>
                <FAB.Group
                    fabStyle={styles.fab}
                    open={this.state.fabOpen}
                    icon={this.state.fabOpen ? 'menu-down' : 'menu-up'}
                    actions={[
                        {
                            icon: 'qrcode-scan',
                            label: 'Scan',
                            onPress: () => { this.props.navigation.navigate('QrCode') },
                            color: 'black'
                        },
                        {
                            icon: 'plus',
                            label: 'Ajouter un bar',
                            onPress: () => { this.props.navigation.navigate('AddBar') },
                            color: 'black'
                        },
                        {
                            icon: 'account',
                            label: 'Mon compte',
                            onPress: () => { this.props.navigation.navigate('DetailUser') },
                            color: 'black'
                        },
                    ]}
                    onStateChange={() => { this.setState({ fabOpen: !this.state.fabOpen }) }}
                />
                {this.state.isVisibleInfos ?
                    <Card style={styles.infos}>
                        <Card.Title
                            title={this.state.barInfo.name}
                            left={LeftContent}
                        />
                        <Card.Content style={{ padding: 5 }}>
                            <Text>{this.state.barInfo.adress}{"\n"}</Text>
                            <Text>{this.state.barInfo.description}{"\n"}</Text>
                        </Card.Content>
                        <Card.Actions style={{ backgroundColor: 'black' }}>
                            <Button
                                color='white'
                                mode='outlined'
                                onPress={() => { this.props.navigation.navigate('DetailBar', { idBar: this.state.barInfo.bar_id }) }}
                                style={{ borderColor: 'white', margin: 5 }}
                            >
                                En savoir plus
                        </Button>
                            <Button
                                onPress={() => { this.manageInfos('close') }}
                                color='white'
                                mode='outlined'
                                style={{ borderColor: 'white' }}
                            >
                                Fermer
                        </Button>
                        </Card.Actions>
                    </Card>
                    : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    map: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: 'yellow'
    },
    infos: {
        position: 'absolute',
        height: '25%',
        width: '90%',
        marginTop: 550,
        marginLeft: 20
    },
    logo: {
        height: 40,
        width: '36%',
    },
    vue: {
        height: '20%',
        width: '10%',
    },
    fab: {
        backgroundColor: 'black'
    }
})

export default HomeComponent;
